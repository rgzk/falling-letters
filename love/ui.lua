local Class = require 'hump.class'
local Timer = require 'hump.timer'

local Button = Class {
	init = function (self, x, y, width, height, text)
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		self.clickAnimationProgress = 0
		self.pressed_x = 0
		self.pressed_y = 0
		self.timer = Timer()
		self.text = text
	end,

	draw = function (self)
		-- Draw the button
		local radius = self.height/4
		love.graphics.setColor(0.8, 0.8, 0.8)
		love.graphics.rectangle('fill', 0, 10, self.width, self.height-10, radius, radius, 100)

		love.graphics.translate(0, self.clickAnimationProgress*7)
		love.graphics.setColor(0.95, 0.95-self.clickAnimationProgress/100, 0.95-self.clickAnimationProgress/100)
		love.graphics.rectangle('fill', 0, 0, self.width, self.height-7, radius, radius, 100)

		self:draw_text()
		love.graphics.translate(0, -self.clickAnimationProgress*7)
		-- Draw the animation
		--love.graphics.setColor(0, 1, 1)
		--love.graphics.circle('fill', self.pressed_x, self.pressed_y, self.clickAnimationProgress*self.width)
	end,

	draw_text = function(self)
		if not self.text then
			return
		end
		love.graphics.setColor(0.3, 0.3, 0.3)
		love.graphics.printf(self.text, 0, self.height/6, self.width, 'center')
	end,

	getPos = function (self)
		return self.x, self.y, self.width, self.height
	end,

	update = function (self, dt)
		self.timer:update(dt)
	end,

	mousepressed = function (self, x, y)
		self.pressed_x = x
		self.pressed_y = y
		self.timer:tween(0.1, self, { clickAnimationProgress = 1.0 }, 'quad', function()
			self.timer:tween(0.4, self, { clickAnimationProgress = 0.0 }, 'quad',
			function()
				if self.clicked then
					self:clicked()
				end
			end)
		end)
	end
}

return {
	Button = Button
}