local Timer = require 'hump.timer'
local Class = require 'hump.class'
local english = require 'english'
local Board = require 'board'
local Scene = require 'commons.scene'

game = {}

local TEXT_CONGRAT = {
	'GREAT',
	'AWESOME',
	'IMPRESSIVE',
	'AMAZING',
	'GENIUS',
	'WOW',
	'NO WAY',
	'ARE YOU CHEATING?'
}

function game:init()
	w, h = love.graphics.getDimensions()
	self.timer = Timer.new()
	self.score = 0
	self.backgroundColor = { 0.3, 0.4, 0.8 }
	self.lastWordPlayed = ''

	self.backgroundProgress = 0.0
	self.backgroundTween = nil
	self.letterFont = love.graphics.newFont('fonts/Righteous-Regular.ttf', 32)

	local game = self

	self.topText = {
		value = '',
		transitionValue = 0,

		show = function (self, value)
			self.value = value
			self.transitionValue = 0
			game.timer:tween(0.3, self, { transitionValue = 1.0 }, 'in-out-quad', function()
				game.timer:tween(1, self, { transitionValue = 2.0})
			end)
		end,

		draw = function (self)
			if self.transitionValue >= 0 then
				love.graphics.setColor(1, 1, 1, self.transitionValue)
			end
			if self.transitionValue > 1 then
				love.graphics.setColor(1, 1, 1, 2 - self.transitionValue)
			end
			love.graphics.printf(self.value, 0, (1-self.transitionValue)*h/16, w, 'center')
		end,

		getPos = function (self)
			return 0, h/8, h/8, w
		end
	}
end

function drawButton(x, y, width, height)
	local radius = height/4
	love.graphics.setColor(0.8, 0.8, 0.8)
	love.graphics.rectangle('fill', x, y, width, height, radius, radius, 100)
	love.graphics.setColor(0.95, 0.95, 0.95)
	love.graphics.rectangle('fill', x, y, width, height - 7, radius, radius, 100)
end

function game:drawBackground()
	local colors = {
		{ 0.4, 0.5, 0.8 },
		{ 0.5, 0.5, 0.8 },
		{ 0.5, 0.6, 0.8 },
		{ 0.5, 0.6, 0.9 },
		{ 0.5, 0.7, 0.9 },
		{ 0.6, 0.7, 0.9 },
		{ 0.6, 0.8, 0.9 }
	}
	for i=0,6 do
		if self.backgroundProgress < i then
			break
		end
		love.graphics.setColor(colors[i+1]) -- 0.4 + i/10, 0.5 + i/10, 0.8 + i/10)
		love.graphics.circle('fill', w/2, h/2, h*(self.backgroundProgress-i))
	end
end

function game:enter()
	local tweenBackground
	tweenBackground = function ()
		local r, g, b = love.math.random(), love.math.random(), love.math.random()
		self.timer:tween(10, self.backgroundColor, { r * 0.5, g * 0.5, b * 0.5}, 'in-out-quad', tweenBackground)
	end
	-- tweenBackground()
	w, h = love.graphics.getDimensions()
	self.scene = Scene()
	self.board = Board(w/10, h/4, w/10*8)
	self.scene:add(self.board, true)
	self.nMovesRemaining = 10
	self.targetScore = 100

	local game = self

	self.moveRemainingButton = {
		draw = function (self)
			local size = h/12
			drawButton(0, 0, size, size)
			love.graphics.setColor(0.3, 0.3, 0.3)
			love.graphics.printf(''..game.nMovesRemaining, 0, 0, size/0.6, 'center', 0, 0.6, 0.6)
			love.graphics.setColor(0.5, 0.5, 0.5)
			love.graphics.printf('moves', 0, 0.47*size, size/0.4, 'center', 0, 0.4, 0.4)
		end,

		getPos = function (self)
			return h/32, h/32, h/12, h/12
		end
	}

	self.pointsButton = {
		width = w - h/12*2 - h/32*4,
		height = h/12,
		x = h/32*2 + h/12,
		y = h/32,

		draw = function (self)
			drawButton(0, 0, self.width, self.height)
			love.graphics.setColor(0.3, 0.3, 0.3)
			love.graphics.printf(game.score..'/'..game.targetScore, 0, 0.01*self.height, self.width, 'center', 0, 1, 1)
		end,

		getPos = function (self)
			return self.x, self.y, self.width, self.height
		end
	}

	self.settingsButton = {
		draw = function (self)
			drawButton(0, 0, h/12, h/12)
		end,

		getPos = function (self)
			return w - h/32 - h/12, h/32, h/32, h/32
		end
	}

	self.lastWordButton = {
		draw = function (self)
			drawButton(0, 0, w - h/32*2, h/12)
			love.graphics.setColor(0.3, 0.3, 0.3)
			love.graphics.printf(game.lastWordPlayed, 0, 0.01*h/12, w - h/32*2, 'center', 0, 1, 1)
		end,

		getPos = function (self)
			return h/32, h - h/32 - h/12, w - h/32*2, h/12
		end
	}

	self.scene:add(self.topText)
	self.scene:add(self.moveRemainingButton)
	self.scene:add(self.pointsButton)
	self.scene:add(self.settingsButton)
	self.scene:add(self.lastWordButton)

	self:resize(w, h)
end

function game:update(dt)
	local code = (love.filesystem.load 'game.lua')()
	for key, prop in pairs(code) do
		if type(prop) == 'function' then
			self[key] = prop
		end
	end
	self.timer:update(dt)
	self.board:update(dt)
end

function game:resize(new_w, new_h)
	local s = h/5
	w, h = new_w, new_h
	self.board.size = math.min(w*0.8 - h/32, h - 2*s)
	self.board.x = (w - self.board.size)/2
	self.board.y = (h - self.board.size)/2
	self.moveRemainingButton.size = h/12
	self.pointsButton.width = w - h/12*2 - h/32*4
	self.pointsButton.height = h/12
	self.pointsButton.x = h/32*2 + h/12
	self.pointsButton.y = h/32
end

function game:mousepressed(x, y)
	self.scene:mousepressed(x,y)
	if #self.board:getSelectedWord() > 0 then
		self.backgroundTween = self.timer:tween(0.3, self, { backgroundProgress = 1})
	end
end

function game:mousemoved(x, y)
	local nLetters = #self.board:getSelectedWord()
	self.scene:mousemoved(x,y)
	if #self.board:getSelectedWord() > nLetters then
		if self.backgroundTween ~= nil then
			self.timer:cancel(self.backgroundTween)
		end
		self.backgroundTween = self.timer:tween(0.3, self, { backgroundProgress = #self.board:getSelectedWord()})
	end
end

function game:mousereleased(x,y)
	self.scene:mousereleased(x,y)
	if self.backgroundTween ~= nil then
		self.timer:cancel(self.backgroundTween)
		-- self.backgroundTween = self.timer:tween(0.3, self, { backgroundProgress = 0 })
	end
	self.backgroundProgress = 0

	-- Get the word played and check it's valid
	local word = english.getWord(self.board:getSelectedWord())
	if word == nil then
		self.board:clearSelectedLetters()
		return
	end

	local wordValue = #word * 5
	self.nMovesRemaining = self.nMovesRemaining - 1
	self.topText:show(TEXT_CONGRAT[math.min(#word-1, #TEXT_CONGRAT)]..'! +'..wordValue)
	self.score = self.score + wordValue
	self.lastWordPlayed = word

	self.board:removeSelectedLetters(function ()
		self.board:applyGravity()

		-- Update the board, putting a joker if the word is longer than 4 characters
		self.board:fillBoard(#word >= 4)
	end)
end


function game:draw()
	love.graphics.setBackgroundColor(self.backgroundColor)
	love.graphics.setColor(self.backgroundColor)
	love.graphics.rectangle('fill', 0, 0, w, h)

	self:drawBackground()
	self.scene:draw()

	love.graphics.setColor(1, 1, 1)

	-- Draw the board
	--self.board:draw()
end

return game