
function love.conf(t)
	ratio = 16/9
	t.window.width = 600
	t.window.resizable = true
	t.window.height = t.window.width * ratio
	t.window.msaa = 2 -- number of samples for multi-sampled aliasing
end