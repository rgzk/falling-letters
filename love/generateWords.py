#!/usr/bin/python

words = []

# Read the words
with open('words.txt', 'r') as f:
	for word in f:
		# Verify if the word is only made up of lowercase letters
		skip = False
		for letter in word[:-1]:
			if ord(letter) < ord('a') or ord(letter) > ord('z'):
				skip = True
		if skip:
			continue
		words.append(word[:-1])

# Print out the words
with open('englishWords.lua', 'w') as f:
	f.write('return {')
	for word in words:
		f.write("['" + word + "'] = true,")
	f.write('}')