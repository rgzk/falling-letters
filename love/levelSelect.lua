local ui = require './ui'
local Timer = require 'hump.timer'
local Scene = require './commons/scene'
local game = require './game'

local levelSelect = {}

function levelSelect:init()
	self.timer = Timer()
	self.starImage = love.graphics.newImage('gfx/star.png')
	self.starFilledImage = love.graphics.newImage('gfx/star-filled.png')
end

function levelSelect:enter()
	w, h = love.graphics.getDimensions()
	self.levelIndex = 0
	self.dialogProgress = 0
	self.leavingAnimationProgress = 0
	self.scene = Scene()
	local s = (w - h/32)/4 - h/32
	self.levelButtons = {}
	for i=1,4 do
		self.levelButtons[i] = {}
		for j=1,4 do
			local x = h/32 + (h/32 + s)*(i-1)
			local y = h/32 + (h/32 + s)*(j-1)
			self.levelButtons[i][j] = ui.Button(x, y, s, s, ''..(i + (j - 1)*4))
			self.levelButtons[i][j].clicked = function()
				self:levelSelected(i + (j - 1)*4)
			end
			self.scene:add(self.levelButtons[i][j])
		end
	end
	self.playButton = ui.Button(w/4, h - h/32, w/2, h/16, 'Play level')
	self.playButton.clicked = function ()
		self.timer:tween(0.6, self, { leavingAnimationProgress = 1.0 }, 'quad', function ()
			Gamestate.switch(game)
		end)
	end
	love.graphics.setBackgroundColor(0.3, 0.4, 0.8)
	self:resize(w, h)
end

function levelSelect:mousepressed(x, y)
	self.scene:mousepressed(x, y)

	-- Check if the "Play level" button is clicked
	if self.dialogProgress >= 0.95 and y >= h/2 + self.playButton.y and x >= self.playButton.x then
		self.playButton:mousepressed(0, 0)
		-- Gamestate.switch(game)
	end
end

function levelSelect:update(dt)
	self.scene:update(dt)
	self.timer:update(dt)
	self.playButton:update(dt)
end

function levelSelect:levelSelected(levelIndex)
	local appear = function()
		self.levelIndex = levelIndex
		self.timer:tween(1.2, self, { dialogProgress = 1.0 }, 'bounce')
	end
	local disappear = function(next)
		self.timer:tween(0.3, self, { dialogProgress = 0.0 }, 'quad', next)
	end
	if self.levelIndex == levelIndex and self.dialogProgress >= 0.5 then
		disappear()
	elseif self.dialogProgress >= 0.99 then
		disappear(appear)
	else
		appear()
	end
end

function levelSelect:resize(new_w, new_h)
	w = new_w
	h = new_h
	self.playButton.width = w/4*3
	self.playButton.height = h/10
	self.playButton.x = (w - self.playButton.width)/2
	self.playButton.y = h/2 - self.playButton.x - self.playButton.height
end

function levelSelect:draw()
	self.scene:draw()

	love.graphics.setColor(0, 0, 0, 0.2*self.dialogProgress)
	love.graphics.rectangle('fill', 0, 0, w, h)

	love.graphics.translate(0, h - h/2*self.dialogProgress)

	love.graphics.setColor(0.5, 0.5, 0.5)
	love.graphics.setLineWidth(4)
	love.graphics.line(0, 0, w, 0)
	love.graphics.setColor(0.2, 0.2, 0.2)
	love.graphics.rectangle('fill', 0, 0, w, h/2)

	love.graphics.setColor(0.9, 0.9, 0.9)
	love.graphics.printf('Level '..self.levelIndex, 0, h/32, w, 'center')

	local img_w = self.starImage:getPixelWidth()/2
	local img_x = (w - img_w*3 - h/16)/2
	love.graphics.draw(self.starImage, img_x, 150, 0, 0.5, 0.5)
	love.graphics.draw(self.starImage, img_x + (img_w + h/32), 150, 0, 0.5, 0.5)
	love.graphics.draw(self.starImage, img_x + (img_w + h/32)*2, 150, 0, 0.5, 0.5)

	love.graphics.translate(self.playButton.x, self.playButton.y)
	self.playButton:draw()
	love.graphics.translate(-self.playButton.x, -self.playButton.y)

	love.graphics.translate(0, -h + h/2*self.dialogProgress)

	if self.leavingAnimationProgress > 0 then
		love.graphics.setColor(0.3, 0.4, 0.8, self.leavingAnimationProgress)
		love.graphics.rectangle('fill', 0, 0, w, h)
	end
end

return levelSelect