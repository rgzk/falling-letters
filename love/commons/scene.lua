local Class = require '../hump.class'

local Scene = Class {
	init = function (self)
		self.objects = {}
		self.fullscreenObjects = {}
		self.nonFullScreenObjects = {}
	end,

	--| Adds an object to the scene. The objects should contains the fields
	--| x, y, width, height.
	add = function (self, obj, isFullScreen)
		self.objects[#self.objects + 1] = obj
		if isFullScreen then
			self.fullscreenObjects[#self.fullscreenObjects + 1] = obj
		else
			self.nonFullScreenObjects[#self.nonFullScreenObjects + 1] = obj
		end
	end,

	draw = function (self)
		for _, obj in ipairs(self.objects) do
			local obj_x, obj_y, w, h = obj:getPos()
			love.graphics.translate(obj_x, obj_y)
			obj:draw()
			love.graphics.translate(-obj_x, -obj_y)
		end
	end,

	objectsAt = function (self, x, y)
		local out = {}
		for _, obj in ipairs(self.nonFullScreenObjects) do
			local obj_x, obj_y, w, h = obj:getPos()
			if x >= obj_x
				and y >= obj_y
				and x <= obj_x + w
				and y <= obj_y + h then
				out[#out+1] = obj
			end
		end
		return out
	end,

	handle_mouse_event = function(self, event, x, y)
		for _, obj in ipairs(self.fullscreenObjects) do
			if obj[event] then
				local obj_x, obj_y = obj:getPos()
				obj[event](obj, x - obj_x, y - obj_y)
			end
		end
		for _, obj in ipairs(self:objectsAt(x, y)) do
			if obj[event] then
				local obj_x, obj_y = obj:getPos()
				obj[event](obj, x - obj_x, y - obj_y)
			end
		end
	end,

	mousepressed = function (self, x, y)
		self:handle_mouse_event('mousepressed', x, y)
	end,

	mousereleased = function (self, x, y)
		self:handle_mouse_event('mousereleased', x, y)
	end,

	mousemoved = function (self, x, y)
		self:handle_mouse_event('mousemoved', x, y)
	end,

	update = function (self, dt)
		for _, obj in ipairs(self.objects) do
			if obj.update then
				obj:update(dt)
			end
		end
	end
}

return Scene