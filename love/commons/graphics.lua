local Class = require '../hump.class'

local graphics = {}

graphics.Scene = Class {
	init = function ()
		self.objects = {}
	end,

	--| Adds an object to the scene. The objects should contains the fields
	--| x, y, width, height.
	add = function (self, obj)
		self.objects[#self.objects] = obj
	end,

	draw = function(self)
		for _, obj in self.objects do
			if obj.draw then
				love.graphics.translate(obj.x, obj.y)
				obj:draw()
				love.graphics.translate(-obj.x, -obj.y
			end
		end
	end
}

return graphics