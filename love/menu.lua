local game = require './game'
local Scene = require './commons/scene'
local ui = require './ui'
local levelSelect = require './levelSelect'

local menu = {}

function menu:init()
	w, h = love.graphics.getDimensions()

	-- Load the resources
	self.backgroundImage = love.graphics.newImage('gfx/mountains.jpg')
	self.font = love.graphics.newFont('fonts/Righteous-Regular.ttf', 48)

	self.leavingAnimationProgress = 0.0
end

function menu:enter()
	self.scrollX = 0

	self.scene = Scene()
	self.playButton = ui.Button(w/4, 200, w/2, h/10, 'Levels')
	self.adventureButton = ui.Button(w/4, 200, w/2, h/10, 'Play')
	local menu = self
	self.playButton.clicked = function (self)
		self.timer:tween(1.0, menu, { leavingAnimationProgress = 1.0}, 'in-out-quad', function()
			Gamestate.switch(levelSelect)
		end)
	end

	self.scene:add(self.playButton)
	self.scene:add(self.adventureButton)
	self:resize(w, h)
end

function printCentered(text, y)
	love.graphics.printf(text, 0, y, w, 'center')
end

function menu:resize(new_w, new_h)
	w = new_w
	h = new_h
	self.playButton.width = w/4*3
	self.playButton.height = h/10
	self.playButton.x = (w - self.playButton.width)/2
	self.playButton.y = h - self.playButton.x - self.playButton.height
	self.adventureButton.width = w/4*3
	self.adventureButton.height = h/10
	self.adventureButton.x = (w - self.playButton.width)/2
	self.adventureButton.y = h - (self.playButton.x + self.playButton.height)*2
	print(self.adventureButton.y)
end

function menu:update(dt)
	self.scrollX = self.scrollX + dt * 32
	self.scene:update(dt)
end

function menu:draw()
	love.graphics.setFont(self.font)
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(self.backgroundImage, 0, 0, 0, 0.5, 0.5)
	self.scene:draw()

	love.graphics.setColor(1, 1, 1)
	love.graphics.printf('Connect', 0, 200, w/1.3, 'center', 0, 1.3, 1.3)
	love.graphics.printf('the', 0, 280, w, 'center')
	love.graphics.printf('Letters', 0, 340, w/1.4, 'center', 0, 1.4, 1.4)

	if self.leavingAnimationProgress > 0 then
		love.graphics.setColor(0.3, 0.4, 0.8, self.leavingAnimationProgress)
		love.graphics.rectangle('fill', 0, 0, w, h)
	end
end

function menu:mousepressed(x, y)
	self.scene:mousepressed(x, y)
end

return menu