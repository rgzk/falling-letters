local english = {}
local words = require('./englishWords')

-- Letter frequency in percent (should sum up to 100)
local letterFrequencies = {
	a = 8.167,
	b = 1.42,
	c = 2.782,
	d = 4.253,
	e = 12.702,
	f = 2.228,
	g = 2.015,
	h = 6.094,
	i = 6.966,
	j = 0.153,
	k = 0.772,
	l = 4.025,
	m = 2.406,
	n = 6.749,
	o = 7.507,
	p = 1.929,
	q = 0.095,
	r = 5.987,
	s = 6.327,
	t = 9.056,
	u = 2.758,
	v = 0.978,
	s = 6.327,
	t = 9.056,
	u = 2.758,
	v = 0.978,
	w = 2.360,
	x = 0.150,
	y = 1.974,
	z = 0.074,
	[' '] = 0
}

local LETTERS = 'abcdefghijklmnopqrstuvwxyz'

local sum = 0
for _, v in pairs(letterFrequencies) do
	sum = sum + v
end

-- Return nil if the word is invalid, and the word itself otherwise.
function english.getWord(word)
	if #word <= 1 then
		return nil
	end

	-- Find where the jokers are located in the word
	local jokers = {}
	local new_word = ''
	for i=1,#word do
		local c = word:sub(i, i)
		if c == ' ' then
			jokers[#jokers+1] = i
			new_word = new_word..'a'
		else
			new_word = new_word..c
		end
	end

	-- Return the word with the jokers replaced, or nil if there is no such word
	local get_word
	get_word = function(i)
		if i > #jokers then
			if words[new_word:lower()] then
				return new_word:upper()
			else
				return nil
			end
		else
			for j=1,26 do
				new_word = new_word:sub(1, jokers[i]-1)..LETTERS:sub(j, j)..new_word:sub(jokers[i]+1,#new_word)
				local res = get_word(i+1)
				if res ~= nil then
					return res
				end
			end
		end
	end

	return get_word(1)
end

--| Generate a random letter according to the English letter frequency
function english.randomLetter()
	local value = love.math.random() * sum
	local cumulative = 0
	for letter, letterFreq in pairs(letterFrequencies) do
		cumulative = cumulative + letterFreq
		if cumulative >= value then
			return letter:upper()
		end
	end

	-- This should not be reached
	return 'z'
end

return english