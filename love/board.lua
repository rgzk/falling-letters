local Timer = require 'hump.timer'
local Class = require 'hump.class'
local english = require './english'

Letter = Class {
	init = function(self, board, i, j)
		self.board = board
		self.i = i
		self.j = j
		self.value = english.randomLetter()
		self.removed = false
		self.selected = false
		self.falling = false
		self.animationCircleProgress = 0
		self.animationFadeProgress = 0
		self.y = (j - 1)*board:cellSize()
		self.animations = {}
		self.radiusProgress = 1
	end,

	disappear = function (self, next)
		self.board._timer:tween(0.2, self, { radiusProgress = 0 }, 'expo', function()
			self.removed = true
			if next then
				next()
			end
		end)
	end,

	appear = function (self)
		self.radiusProgress = 0
		self.board._timer:tween(0.4, self, { radiusProgress = 1.0 }, 'bounce')
	end,

	select = function(self)
		self:cancelAnimations()
		self.selected = true
		self.animationCircleProgress = 0
		self.animationFadeProgress = 0
		self.animations[1] = self.board._timer:tween(0.4, self, { animationCircleProgress = 1.0 }, 'bounce')
		self.animations[2] = self.board._timer:tween(0.8, self, { animationFadeProgress = 1.0 })
	end,

	cancelAnimations = function (self)
		for _, anim in ipairs(self.animations) do
			self.board._timer:cancel(anim)
		end
	end,

	unselect = function(self)
		self:cancelAnimations()
		self.selected = false
		self.animationCircleProgress = 0
		self.animationFadeProgress = 0
	end,

	draw = function(self)
		if self.removed then
			return
		end
		local cellSize = self.board:cellSize()
		local x = (self.i - 1) * cellSize
		local textX = math.floor(x + cellSize*0.01)
		local textY = math.floor(self.y + cellSize*0.18)
		local centerX = x + cellSize/2
		local centerY = self.y + cellSize/2

		-- Draw the animation circle
		if self.animationFadeProgress > 0 then
			local a = 1 - self.animationFadeProgress
			love.graphics.setColor(1, 1, 1, a)
			love.graphics.circle('fill', centerX, centerY, cellSize*(self.animationCircleProgress*0.2+0.4))
		end

		-- Draw the cell itself
		if self.selected then
			love.graphics.setColor(0.8, 0.4, 0.4)
		else
			love.graphics.setColor(0.8, 0.8, 0.8)
		end
		love.graphics.circle('fill', centerX, centerY, cellSize*0.4*self.radiusProgress, 400)
		if self.selected then
			love.graphics.setColor(0.95, 0.4, 0.4)
		else
			love.graphics.setColor(0.95, 0.95, 0.95)
		end
		love.graphics.arc('fill', centerX, centerY - 5, cellSize*0.4*self.radiusProgress, math.pi*0.99, -0.01, 400)
		love.graphics.arc('fill', centerX, centerY, cellSize*0.4*self.radiusProgress, -math.pi, 0, 400)
		if self.selected then
			love.graphics.setColor(0.95, 0.95, 0.95)
		else
			love.graphics.setColor(0.3, 0.3, 0.3)
		end
		love.graphics.printf(self.value, textX, textY, cellSize, 'center')
	end
}

Board = Class {
	--| Create a new square board located at (x,y) with the given size.
	init = function (self, x, y, size)
		-- Size and coordinate of the board in pixels
		self.size = size
		self.x = x
		self.y = y

		-- Font used to draw the letters
		self.letterFont = love.graphics.newFont('fonts/Righteous-Regular.ttf', 32)
		self._timer = Timer.new()

		-- Whether the player is selecting a word, and the coordinate of the mouse
		self.selecting = false
		self._mouseX = nil
		self._mouseY = nil

		-- Initialize the board randomly
		self._selectedLetters = {}
		self._letters = {}
		for i=1,5 do
			self._letters[i] = {}
			for j=1,5 do
				self._letters[i][j] = Letter(self, i, j)
				self._letters[i][j]:appear()
			end
		end
	end
}

function Board:getPos()
	return self.x, self.y, self.size, self.size
end

function Board:mousepressed(x, y)
	local letter = self:getLetterAtCoords(x, y)
	if letter == nil then
		return
	end
	self._mouseX = x
	self._mouseY = y
	self.selecting = true
	self:selectLetter(letter)
end

function Board:mousemoved(x, y)
	-- If we're not selecting a word, we don't care
	if not self.selecting then
		return
	end

	-- Update the mouse position
	self._mouseX = x
	self._mouseY = y

	-- Get the letter under the finger
	local letter = self:getLetterAtCoords(x, y)
	if letter == nil then
		return
	end

	-- Unselect the last letter if we go back...
	if #self._selectedLetters > 1 and letter == self._selectedLetters[#self._selectedLetters-1] then
		self:unselectLastLetter()
	end
	-- ... or select it if we can
	if math.abs(letter.i - self._selectedLetters[#self._selectedLetters].i) <= 1
		and math.abs(letter.j - self._selectedLetters[#self._selectedLetters].j) <= 1 then
		self:selectLetter(letter)
	end
end

function Board:mousereleased(x, y)
	self.selecting = false
end

function Board:removeSelectedLetters(next)
	for _, letter in ipairs(self._selectedLetters) do
		letter:unselect()
		letter:disappear()
	end
	self._selectedLetters = {}
	self._timer:after(0.3, next)
end

function Board:getSelectedWord()
	local word = ''
	for _, letter in ipairs(self._selectedLetters) do
		word = word .. letter.value
	end
	return word
end

function Board:applyGravity()
	for i=1,5 do
		for j=5,2,-1 do
			if self._letters[i][j].removed then
				for k=j-1,1,-1 do
					if not self._letters[i][k].removed then
						self._letters[i][j] = self._letters[i][k]
						self._letters[i][j].j = j
						self._letters[i][j].falling = true
						self._timer:tween(0.3*(j - k), self._letters[i][j], {
							y = self:cellSize() * (j-1)
						}, 'bounce', function ()
							self._letters[i][j].falling = false
						end)
						self._letters[i][k] = {
							removed = true,
							i = i,
							j = k,
							falling = false,
							selected = false
						}
						break
					end
				end
			end
		end
	end
end

function Board:fillBoard(dropJokers)
	-- Get the number of empty cells
	local nEmptyCells = 0
	for i=1,5 do
		local k = 0
		for j=5,1,-1 do
			if self._letters[i][j].removed then
				nEmptyCells = nEmptyCells + 1
			end
		end
	end
	-- Select the index of the 
	local jokerIndex
	if dropJokers then
		jokerIndex = love.math.random(1, nEmptyCells)
	else
		jokerIndex = -1
	end

	-- Fill the board
	local index = 0
	for i=1,5 do
		local k = 0
		for j=5,1,-1 do
			if self._letters[i][j].removed then
				index = index + 1
				k = k + 1
				self._letters[i][j] = Letter(self, i, j)
				self._letters[i][j].y = self:cellSize()*(-k)
				if index == jokerIndex then
					self._letters[i][j].value = ' '
				end
				self._letters[i][j].falling = true
				self._timer:tween(0.3*(j + k - 1), self._letters[i][j], {
					y = self:cellSize() * (j-1)
				}, 'bounce', function()
					self._letters[i][j].falling = false
				end)
			end
		end
	end
end

function Board:dropJoker()
	-- Get the number of empty cells
	local nEmptyCells = 0
	for i=1,5 do
		local k = 0
		for j=5,1,-1 do
			if self._letters[i][j].removed then
				nEmptyCells = nEmptyCells + 1
			end
		end
	end
	-- Randomly drop a letter in one of the position
	local m = love.math.random(1, nEmptyCells)
	local k = 0
	for i=1,5 do
		for j=5,1,-1 do
			if self._letters[i][j].removed then
				k = k + 1
				if k > m then
					self._letters[i][j] = Letter(self, i, j)
					self._letters[i][j].value = ' '
					self._letters[i][j].y = self:cellSize()*(-k)
					self._letters[i][j].falling = true
					self._timer:tween(0.3*(j + k - 1), self._letters[i][j], {
						y = self:cellSize() * (j-1)
					}, 'bounce', function()
						self._letters[i][j].falling = false
					end)
					return
				end
			end
		end
	end
end

--| Get the letter at the given position or nil.
function Board:getLetterAtCoords(x, y)
	-- Get the index of the letter potentially pressed
	local cellSize = self:cellSize()
	local i, j = math.floor(x/cellSize) + 1, math.floor(y/cellSize) + 1
	if i < 1 or j < 1 or i > 5 or j > 5 then
		return
	end

	-- Verify that the coordinate intersect with the circle
	local dx = (i-0.5)*cellSize - x
	local dy = (j-0.5)*cellSize - y
	if dx*dx + dy*dy <= (cellSize*0.4)*(cellSize*0.4) then
		return self._letters[i][j]
	else
		return nil
	end
end

--| Return the size in pixels of a singel cell
function Board:cellSize()
	return self.size / 5
end

--| Mark the letter as selected
function Board:selectLetter(letter)
	if letter.selected or letter.falling then
		return
	end
	self._selectedLetters[#self._selectedLetters+1] = letter
	letter:select()
end

function Board:unselectLastLetter()
	self._selectedLetters[#self._selectedLetters]:unselect()
	self._selectedLetters[#self._selectedLetters] = nil
end

function Board:clearSelectedLetters()
	for _, letter in ipairs(self._selectedLetters) do
		letter:unselect()
		letter.animationSelectedProgress = 0
		letter.animationFade = 0
	end
	self._selectedLetters = {}
end

function Board:update(dt)
	local code = (love.filesystem.load 'board.lua')()
	for key, prop in pairs(code) do
		if type(prop) == 'function' then
			self[key] = prop
		end
	end
	self._timer:update(dt)
	if self._oldSize ~= self.size then
		for i=1,5 do
			for j=1,5 do
				self._letters[i][j].y = (j-1) * self:cellSize()
			end
		end
		self.letterFont = love.graphics.newFont('fonts/Righteous-Regular.ttf', math.floor(self:cellSize()/2))
		self._oldSize = self.size
	end
end

--| Draw the board entirely
function Board:draw()
	love.graphics.setFont(self.letterFont)
	local cellSize = self:cellSize()
	local fontScale = cellSize / 64

	-- Draw the lines between selected letters
	love.graphics.setColor(1.0, 1.0, 1.0)
	if self.selecting then
		love.graphics.setLineWidth(cellSize/10)
		for i=2, #self._selectedLetters do
			local i1, j1 = self._selectedLetters[i-1].i, self._selectedLetters[i-1].j
			local i2, j2 = self._selectedLetters[i].i, self._selectedLetters[i].j
			love.graphics.line(
				i1*cellSize - cellSize/2,
				j1*cellSize - cellSize/2,
				i2*cellSize - cellSize/2,
				j2*cellSize - cellSize/2)
		end
		local lastLetter = self._selectedLetters[#self._selectedLetters]
		love.graphics.line((lastLetter.i - 0.5)*cellSize, (lastLetter.j - 0.5)*cellSize, self._mouseX, self._mouseY)
	end

	-- Draw the letters
	for i=1,5 do
		for j=1,5 do
			local letter = self._letters[i][j]
			if not letter.removed then
				local x, y = (i - 1)*cellSize, letter.y
				letter:draw()
			end
		end
	end
end

return Board